import angular from 'angular';
import 'angular-material';
import { Component } from 'angular-ts-decorators';

@Component({
    selector: 'app',
    templateUrl: 'app/app.view.html',
})
export class AppComponent {
    public static $inject: string[] = [
        '$log',
        '$scope'
    ];

    public name: string = '';
    public lastMessage: string = '';

    constructor(
        private $log: angular.ILogService,
        private $scope: angular.IRootScopeService) {
        this.$log.debug(`${AppComponent.name}::ctor`);
    }

    public sayHello() {
        this.$log.debug(`${AppComponent.name}::sayHello`);
        const w: any = window;
        w.CordovaPluginJavaConnection.sayHello(this.name,
            (resp) => {
                this.$log.debug(`${AppComponent.name}::sayHello (Success) %o`, resp);
                alert(`Respuesta: ${resp}`)
            },
            () => {
                this.$log.debug(`${AppComponent.name}::sayHello (Error)`);
            })
    }

    public registerEvents() {
        this.$log.debug(`${AppComponent.name}::registerEvents`);
        const w: any = window;
        w.CordovaPluginJavaConnection.registerEventLog(
            (message) => {
                this.$log.debug(`${AppComponent.name}::registerEventLog %o`, message);
                this.$scope.$apply(() => {
                    this.lastMessage = message;
                });
            });
    }
}