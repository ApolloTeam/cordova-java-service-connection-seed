package com.prueba.conex;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Message service
 * - Recive los mensajes del Servicio de transmisión y se los envia al plugin.
 * - Resuelve la comunicación entre procesos.
 *
 * @author Damián Eiff  eiff.damian@gmail.com
 */
public class MessageService extends IntentService {

    private static final String TAG = MessageService.class.getSimpleName();

    public static final String ACTION_EVENT_MESSAGE = "ACTION_EVENT_MESSAGE";

    public static final String EXTRA_DATA = "EXTRA_DATA";

    public MessageService() {
        super("MessageService");
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    /**
     * Activación del servicio.
     *
     * @param intent  Intención a ejecutar.
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String threadName = Thread.currentThread().getName();     
        String methodName = "onStartCommand TH: " + threadName;
        if (intent != null) {
            // Obtiene el nombre de la acción a ejecutar.
            String action = intent.getAction();
            Log.d(TAG, methodName + " intent: " + action + " flags: " + flags + " startId: " + startId);
            if (ACTION_EVENT_MESSAGE.equals(action)) {
                String message = intent.getExtras().getString(EXTRA_DATA);
                CordovaPluginJavaConnection.publishEvent(message);

            } else {
                Log.d(TAG, methodName + " intent: UNKNOWN ACTION flags: " + flags + " startId: " + startId);
            }
        } else {
            Log.d(TAG, methodName + " intent: NO ACTION flags: " + flags + " startId: " + startId);
        }

        return super.onStartCommand(intent, flags, startId);
    }
}