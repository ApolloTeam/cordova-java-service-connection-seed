package com.prueba.conex;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.traslada.cordovaPluginJavaConnection.MainActivity;

/**
 * Message service
 * - Recive los mensajes del Servicio de transmisión y se los envia al plugin.
 * - Resuelve la comunicación entre procesos.
 *
 * @author Damián Eiff  eiff.damian@gmail.com
 */
public class BackgroundService extends Service {

    public static final String ACTION_INIT = "ACTION_INIT";

    private static final String NOTIFICATION_CHANNEL_ID = "10001";
    private static final int NOTIFICATION_ID = 10001;

    // TAG para el Log.
    private static final String TAG = BackgroundService.class.getSimpleName();

    // Create the Handler object (on the main thread by default)
    // Fix execution thread.
    // Link: https://stackoverflow.com/a/11125271
    private final Handler handler = new Handler(Looper.getMainLooper());

    // Define the code block to be executed
    private final Runnable mainLoopTask = new Runnable() {
        @Override
        public void run() {
            // Link: https://guides.codepath.com/android/Repeating-Periodic-Tasks
            String threadName = Thread.currentThread().getName();
            String methodName = "mainLoopTask TH: " + threadName;
            try {

                Intent intent = new Intent(BackgroundService.this, MessageService.class);
                Bundle bundle = new Bundle();
                bundle.putString(MessageService.EXTRA_DATA, "MESSAGE " + System.currentTimeMillis());
                intent.setAction(MessageService.ACTION_EVENT_MESSAGE);
                intent.putExtras(bundle);
                BackgroundService.this.startService(intent);

            } catch (Exception ex) {
                Log.e(TAG, methodName + " ex: " + ex.getMessage());
                ex.printStackTrace();
            } finally {
                // Se ejecuta cada 10 segundos.
                handler.postDelayed(this, 10 * 1000);
            }
        }
    };


    /**
     * Contructor
     */
    public BackgroundService() {
        super();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    /**
     * Activación del servicio.
     * @param intent  Intención a ejecutar.
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String threadName = Thread.currentThread().getName();
        String methodName = "onStartCommand TH: " + threadName;
        if (intent != null) {
            // Obtiene el nombre de la acción a ejecutar.
            String action = intent.getAction();
            Log.d(TAG, methodName + " intent: " + action + " flags: " + flags + " startId: " + startId);
            if (ACTION_INIT.equals(action)) {
                // Se ejecuta cada 10 segundos.
                handler.postDelayed(mainLoopTask, 10 * 1000);
                createNotification("Prueba", "Servicio de prueba");

            } else {
                Log.d(TAG, methodName + " intent: UNKNOWN ACTION flags: " + flags + " startId: " + startId);
            }
        } else {
            Log.d(TAG, methodName + " intent: NO ACTION flags: " + flags + " startId: " + startId);
        }

        return Service.START_STICKY;
    }

    /**
     * Create and push the notification
     */
    private void createNotification(String title, String message) {
        /**Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(this,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder;
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert notificationManager != null;

            notificationManager.createNotificationChannel(notificationChannel);
            builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
        } else {
            builder = new NotificationCompat.Builder(this);
        }

        builder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(false)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        assert notificationManager != null;

        // notificationManager.notify(NOTIFICATION_ID , builder.build());
        startForeground(NOTIFICATION_ID, builder.build());
    }
}