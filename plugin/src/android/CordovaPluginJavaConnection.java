package com.prueba.conex;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PermissionHelper;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Main plugin class
 */
public class CordovaPluginJavaConnection extends CordovaPlugin {

    private static final String TAG = CordovaPluginJavaConnection.class.getSimpleName();

    private static CallbackContext sayHelloContext = null;
    
    private static CallbackContext eventLoggerContext = null;    

    private static CordovaPluginJavaConnection self = null;

    /**
     * Plugin initialization
     * - Creates configuration
     * - Register Receiver to communicate Service with Cordova Plugin
     */
    @Override
    protected void pluginInitialize() {
        init();
    }

    @Override
    public void onReset() {
        CordovaPluginJavaConnection.sayHelloContext = null;
    }

    /**
     * Javascript callbacks execution context
     * @param action          The action to execute.
     * @param args            The exec() arguments.
     * @param callbackContext The callback context used when calling back into JavaScript.
     * @return
     * @throws JSONException
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        boolean ret = false;
        try {
            if (action.equals("sayHello")) {
                sayHello(args, callbackContext);
                ret = true;
            } else if (action.equals("registerEventLog")) {
                registerEventLog(callbackContext);
                ret = true;
            }

            
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return ret;
    }

    /**
     * Publish messages to the application in a broadcast channel
     *
     * @param message
     */
    public static void publishEvent(String message) {
        String methodName = "publishEvent";
        Log.d(TAG, methodName + " message: " + message);
        try {
            if (self != null) {
                final Activity context = self.cordova.getActivity();
                final CallbackContext callback = CordovaPluginJavaConnection.eventLoggerContext;
                if (callback != null) {
                    PluginResult r = new PluginResult(PluginResult.Status.OK, message);
                    r.setKeepCallback(true);
                    callback.sendPluginResult(r);
                }

            }
        } catch (Exception ex) {
            Log.e(TAG, methodName + " ex: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Notifica a la app que se completo correctamente la acción sayHello en el servicio.
     */
    public static void sayHelloComplete() {
        final CallbackContext callbackContext = CordovaPluginJavaConnection.sayHelloContext;
        if (callbackContext != null) {
            PluginResult r = new PluginResult(PluginResult.Status.OK, Boolean.TRUE);
            callbackContext.sendPluginResult(r);
        }
    }
    
    /**
     * Notifica a la app que ocurrio un error.
     */
    public static void sayHelloFail(String message) {
        final CallbackContext callbackContext = CordovaPluginJavaConnection.sayHelloContext;
        if (callbackContext != null) {
            PluginResult r = new PluginResult(PluginResult.Status.ERROR, message);
            callbackContext.sendPluginResult(r);
        }
    }

    /**
     * Say Hello.
     * @param args
     * @param callbackContext
     */
    private void sayHello(final JSONArray args, final CallbackContext callbackContext) {
        final Activity context = cordova.getActivity();
        this.cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                try {

                    String value = getStringSafe(args, 0);
                    String msg = "Hola " +  value;
                    sayHelloContext(callbackContext, msg);

                } catch (Exception ex) {
                    ex.printStackTrace();
                    if (callbackContext != null) {
                        callbackContext.error(ex.getMessage());
                    }

                    sayHelloContext(null, "");
                }
            }
        });
    }

    /**
     * Registra el callback y activa el servicio.
     * @param callbackContext
     */
    private void registerEventLog(final CallbackContext callbackContext) {
        final Activity context = cordova.getActivity();
        this.cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                try {                    
                    setEventLogContext(callbackContext);
                    Intent intent = new Intent(context, BackgroundService.class);
                    intent.setAction(BackgroundService.ACTION_INIT);                    
                    customStartService(context, intent);

                } catch (Exception ex) {
                    Log.d(TAG, "registerEventLog ex: " + ex.getMessage());
                    ex.printStackTrace();
                    if (callbackContext != null) {
                        callbackContext.error(ex.getMessage());
                    }
                }
            }
        });
    }

    private void setEventLogContext(final CallbackContext callbackContext) {
        CordovaPluginJavaConnection.eventLoggerContext = callbackContext;
        if (CordovaPluginJavaConnection.eventLoggerContext != null) {
            PluginResult r = new PluginResult(PluginResult.Status.OK);
            r.setKeepCallback(true);
            CordovaPluginJavaConnection.eventLoggerContext.sendPluginResult(r);
        }
    }

    /**
     * Guarda la referencia del contexto de la app web para enviar la respuesta a la llamada.
     * @param callbackContext COntexto de la app web.
     */
    private void sayHelloContext(CallbackContext callbackContext, String msg) {
        // Log.d(TAG, "sayHelloContext TH: " + Thread.currentThread().getName() + " PC: " + this.getCurrentProcessName());
        CordovaPluginJavaConnection.sayHelloContext = callbackContext;

        if (CordovaPluginJavaConnection.sayHelloContext != null) {
            PluginResult r = new PluginResult(PluginResult.Status.OK, msg);
            r.setKeepCallback(true);
            CordovaPluginJavaConnection.sayHelloContext.sendPluginResult(r);
        }
    }
    
    /**
     * Devuelve una cadena del JSONArray o una cadena vacia.
     * @param args  Array de datos en formato JSON.
     * @param index Posición que se esta buscando.
     * @return Una cadena del JSONArray o una cadena vacia.
     */
    private String getStringSafe(final JSONArray args, final int index) {
        try {
            return args.getString(index);
        } catch (JSONException e) {
            return "";
        }
    }

    /**
     * Inicializa el servicio.
     */
    private void init() {
        Log.d(TAG, "init TH: " + Thread.currentThread().getName());
        self = this;
    }

    private static void customStartService(final Context context, final Intent intent) {
        // Link: https://stackoverflow.com/a/48283015
        // Link: https://hackernoon.com/android-location-tracking-with-a-service-80940218f561
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            context.startService(intent);
        } else {
            context.startForegroundService(intent);
        }
    }
}