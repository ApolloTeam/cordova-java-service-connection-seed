var exec = require('cordova/exec');

exports.sayHello = function (arg0, success, error) {
  exec(success, error, 'CordovaPluginJavaConnection', 'sayHello', [arg0]);
};

exports.sayHello = function (arg0, success, error) {
  exec(success, error, 'CordovaPluginJavaConnection', 'sayHello', [arg0]);
};

/**
 * Registra un canal para recibir notificaciones de eventos del servicio
 */
exports.registerEventLog = function (eventHandler) {
  exec(eventHandler, null, 'CordovaPluginJavaConnection', 'registerEventLog');
};